import { Component, OnInit } from '@angular/core';
import { Touchpoint } from 'src/app/models/touchpoint';

@Component({
  selector: 'app-touchpoints',
  templateUrl: './touchpoints.component.html',
  styleUrls: ['./touchpoints.component.scss'],
})
export class TouchpointsComponent implements OnInit {

  public touchpoints: Touchpoint[];
  public touchpoint: Touchpoint;
  constructor() { }

  ngOnInit() {
    // // this.touchpoint.id = 1;
    // this.touchpoints.push(this.touchpoint);
  }

}
