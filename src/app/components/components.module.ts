import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";
import { CurrentSlideComponent } from './public/current-slide/current-slide.component';
import { NotesComponent } from './public/notes/notes.component';
import { TouchpointsComponent } from './public/touchpoints/touchpoints.component';

@NgModule({
    declarations: [
        CurrentSlideComponent,
        TouchpointsComponent,
        NotesComponent,
    ],
    imports: [CommonModule, IonicModule, FormsModule],
    exports: [
        CurrentSlideComponent,
        NotesComponent,
        TouchpointsComponent
    ],
    providers: [],
})
export class ComponentsModule { }
