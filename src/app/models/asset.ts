import { AssetType } from './assetType';

export class Asset {
  constructor(
    public id: number,
    public content: string,
    public mime: string,
    public name: string,
    public type: AssetType,
  ) {}
}
