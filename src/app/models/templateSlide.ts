import { Template } from './template';
import { SlideType } from './slideType';
import { Animations } from './animations';

export class TemplateSlide {
  id: number;
  name: string;
  content: string;
  template: Template;
  templateId: number;
  type: SlideType;
  animations: Animations;
}
