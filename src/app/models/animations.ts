import { Dependency } from './dependency';

export class Animations {
  id: number;
  init: string;
  intro: string;
  loop: string;
  outro: string;
  dependency: Dependency[];
}
