export enum TouchpointType {
  LINK,
  URL,
  IFRAME,
}
