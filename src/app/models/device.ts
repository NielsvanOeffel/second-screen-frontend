export class Device {
  id: number;
  name: string;
  status: string;
  type: string;
  lastOnline: Date;
}
