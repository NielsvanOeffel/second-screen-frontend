import { UserVisibility } from './userVisibility';
import { DepartmentVisibility } from './departmentVisibility';

export class Visibility {
  id: number;
  visibleOrganisation: boolean;
  userVisibilities: UserVisibility[];
  departmenVisibilities: DepartmentVisibility[];
}
