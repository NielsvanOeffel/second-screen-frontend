export class RfidTag {
  RFID_CHIP: number;
  organisationID: number;
  name: string;
}
