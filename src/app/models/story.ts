import { Slide } from './slide';
import { Asset } from './asset';
import { Visibility } from './visibility';

export class Story {
  id: number;
  name: string;
  created: Date;
  updated: Date;
  templateId: number;
  slides: Slide[];
  assets: Asset[];
  visibility: Visibility;
  modelId: number;
}
