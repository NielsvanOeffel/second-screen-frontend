import { Slide } from './slide';
import { TouchpointType } from './touchpointType';
import { TouchpointStyle } from './touchpointStyle';
import { TouchPointObject } from './touchPointObject';
import { TouchpointDeviceType } from './touchpointDeviceType';
import { TouchPointTag } from './touchpointTag';

export class Touchpoint {
  id: number;
  x: number;
  y: number;
  width: number;
  height: number;
  gridLocationIndex: number;
  nextSlide: Slide;
  nextSlideId: number;
  parentSlide: Slide;
  isInvisible: boolean;
  parentSlideId: number;
  type: TouchpointType;
  link: string;
  style: TouchpointStyle;
  touchpointObjects: TouchPointObject[];
  touchpointTags: TouchPointTag[];
  deviceType: TouchpointDeviceType;
}
