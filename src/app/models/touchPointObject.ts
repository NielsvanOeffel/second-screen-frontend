export class TouchPointObject {
  touchPointId: number;
  nextSlideId: number;
  objectId: number;

  constructor(touchpointId: number, nextSlideId: number, objectId: number = null) {
    this.touchPointId = touchpointId;
    this.nextSlideId = nextSlideId;
    this.objectId = objectId;
  }
}
