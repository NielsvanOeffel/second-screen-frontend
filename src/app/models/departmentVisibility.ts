import { Visibility } from './visibility';

export class DepartmentVisibility {
  visibilityId: number;
  departmentId: number;
  visibility: Visibility;
}
