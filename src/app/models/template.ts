import { TemplateSlide } from './templateSlide';
import { TouchpointStyle } from './touchpointStyle';

export class Template {
  id: number;
  name: string;
  templateSlides: TemplateSlide[];
  touchpointStyle: TouchpointStyle[];
}
