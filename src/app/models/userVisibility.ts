import { Visibility } from './visibility';

export class UserVisibility {
  visibilityId: number;
  userId: number;
  visibility: Visibility;
}
