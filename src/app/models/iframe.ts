import { Slide } from './slide';

export class Iframe {
  id: number;
  x: number;
  y: number;
  width: number;
  height: number;
  parentSlide: Slide;
  parentSlideId: number;
  link: string;
}
