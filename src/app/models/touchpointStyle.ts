import { TouchpointHandleAspectRatio } from './touchpointHandleAspectRatio';
import { Animations } from './animations';

export class TouchpointStyle {
  id: number;
  name: string;
  content: string;
  default: boolean;
  handleAspectRatio: TouchpointHandleAspectRatio;
  animations: Animations;
}
