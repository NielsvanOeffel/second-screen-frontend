import { DependencyType } from './dependencyType';

export class Dependency {
  id?: number;
  content: string;
  from?: string;
  type: DependencyType;
}
