export class Model {
  MID: number;
  Date: Date;
  Name: string;
  URL: string;
  Object: any[];
}
