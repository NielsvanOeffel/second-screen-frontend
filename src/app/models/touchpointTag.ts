export class TouchPointTag {
  touchPointId: number;
  nextSlideId: number;
  tagId: number;

  constructor(touchpointId: number, nextSlideId: number, tagId: number = null) {
    this.touchPointId = touchpointId;
    this.nextSlideId = nextSlideId;
    this.tagId = tagId;
  }
}
