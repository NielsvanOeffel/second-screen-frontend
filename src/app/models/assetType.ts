export enum AssetType {
  ORIGINAL_IMAGE,
  ORIGINAL_ICON,
  SVG_IMAGE,
  SVG_ICON,
  FONT,
}
