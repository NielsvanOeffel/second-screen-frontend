import { Story } from './story';
import { Touchpoint } from './touchpoint';
import { SlideType } from './slideType';
import { Notes } from './notes';
import { Iframe } from './iframe';
import { Animations } from './animations';
import { Graph } from './graph';

export class Slide {
  id: number;
  content: string;
  slideType: SlideType;
  story: Story;
  storyId: number;
  touchpoints: Touchpoint[];
  iframes: Iframe[];
  animations: Animations;
  notes: Notes;
  data: string;
}
